
#### 项目介绍
非常简单的一个微服务项目

参考网上资料

#### 软件架构
软件架构说明
暂时只是实现了一些简单的功能

1. 服务治理使用consul
2. api网关使用ocelot
3. 身份认证使用 IdentityServer4 JWT
4. 熔断降级使用Polly 
5. AOP 框架使用AspectCore
6. 服务调用使用 如鹏封装Http请求 和 WebApiClient.JIT

#### 使用说明

1. 运行consul软件
2. 运行OrderService  订单服务
3. 运行LoginService  登录服务
4. 运行AuthenticationService 认证服务
5. 运行ApiGateway 网关项目
6. 运行WebApp项目

#### 下一步
1. 日志收集管理
2. 配置中心
3. 结合ABP项目