﻿using System;
using System.Collections.Generic;
using System.Text;
using Zhj.Somethings.SampleService.Domain.Entity;
using System.Linq;
using Zhj.Somethings.SampleService.Domain.IRepository;

namespace Zhj.Somethings.SampleService.Domain.Cope
{
    public class CopeServiceImpl : ICopeService
    {
        public IProductRepository _productRepository { set; get; }

        public CopeServiceImpl(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public List<Product> GetProducts()
        {
            return _productRepository.GetAll();
        }
    }
}
