﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Zhj.Somethings.SampleService.Domain.Entity;
using Zhj.Somethings.SampleService.Domain.IRepository;
using System.Linq;

namespace Zhj.Somethings.SampleService.Repository
{
    public class BaseRepository<TEntity, TPrimaryKey> : IBaseRepository<TEntity, TPrimaryKey> where TEntity : BaseEntity<TPrimaryKey>, new()
    {
        DbContext db = new MyDbContext();

        public bool Add(TEntity T)
        {
            throw new NotImplementedException();
        }

        public bool Delete(TPrimaryKey id)
        {
            throw new NotImplementedException();
        }

        public TEntity Get(TPrimaryKey id)
        {
            throw new NotImplementedException();
        }

        public List<TEntity> GetAll()
        {
            return db.Set<TEntity>().ToList();
        }

        public bool Update(TEntity T)
        {
            throw new NotImplementedException();
        }
    }
}
