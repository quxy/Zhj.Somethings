﻿using System;
using System.Collections.Generic;
using System.Text;
using Zhj.Somethings.SampleService.Domain.Entity;

namespace Zhj.Somethings.SampleService.Application.Cope
{
    public interface ICopeApplication
    {
        List<Product> GetProducts();
    }
}
