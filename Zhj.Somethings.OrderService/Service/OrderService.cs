﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zhj.Somethings.OrderApi.Dto;

namespace Zhj.Somethings.OrderService.Service
{
    public interface OrderService
    {
        IList<OrderDto> GetOrderList();

        IList<OrderDto> GetOrderListSpare();
    }
}
