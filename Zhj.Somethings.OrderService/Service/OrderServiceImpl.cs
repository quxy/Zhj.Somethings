﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zhj.Common.HystrixCore;
using Zhj.Somethings.OrderApi.Dto;

namespace Zhj.Somethings.OrderService.Service
{
    public class OrderServiceImpl : OrderService
    {

        [HystrixCommand(nameof(GetOrderListSpare))]
        public virtual IList<OrderDto> GetOrderList()
        {
            throw new Exception("异常了");
            IList<OrderDto> list = new List<OrderDto>()
            {
                new OrderDto(){Id =1,Money=100.0, Title="电脑"},
                new OrderDto(){Id =2,Money=100.0, Title="电脑2"}
            };
            return list;
        }

        public virtual IList<OrderDto> GetOrderListSpare()
        {
            IList<OrderDto> list = new List<OrderDto>()
            {
                new OrderDto(){Id =1,Money=100.0, Title="电脑3"},
                new OrderDto(){Id =2,Money=100.0, Title="电脑4"}
            };

            return list;
        }

    }
}
