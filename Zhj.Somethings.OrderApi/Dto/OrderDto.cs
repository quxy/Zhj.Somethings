﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zhj.Somethings.OrderApi.Dto
{
    public class OrderDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public double Money { get; set; }
    }
}
